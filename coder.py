
from wavelet import Wavelet
import numpy as np
import struct
import math
from tempfile import TemporaryFile
import sys
from scipy import signal
import numpy as np
from random import random
import matplotlib.pyplot as plt
import os
from mpl_toolkits.mplot3d import Axes3D

def avgSqDiff(one,two):
  if len(one) != len(two):
    return -1
  s = 0
  for i in range(len(one)):
    s += (one[i]-two[i])**2
  return math.sqrt(s/len(one))

def plot3d(x,y,z):
  fig = plt.figure()
  ax = fig.gca(projection='3d')

  ax.plot(x, y, z, label='000')
  ax.legend()

  plt.show()

def pearson(x,y):
    n=len(x)
    vals=range(n)
    # Простые суммы
    sumx=sum([float(x[i]) for i in vals])
    sumy=sum([float(y[i]) for i in vals])
    # Суммы квадратов
    sumxSq=sum([x[i]**2.0 for i in vals])
    sumySq=sum([y[i]**2.0 for i in vals])
    # Сумма произведений
    pSum=sum([x[i]*y[i] for i in vals])
    # Вычисляем коэффициент корреляции Пирсона
    num=pSum-(sumx*sumy/n)
    den=((sumxSq-pow(sumx,2)/n)*(sumySq-pow(sumy,2)/n))**.5
    if den==0: return 0
    r=num/den
    return r

def pattern(x):
  
  return math.sin(2*math.pi*x)

def includeWindow(n,arr,bit, a):
  arrSin = []
  arrCos = []
  maxVal = max(arr)
  for x in range(n):  
    p = pattern(x/n)
    arrSin.append(p)
    arrCos.append(-p)

    w =  0 if maxVal == 0 else arr[x]/maxVal
    arr[x] = w*(1-a) + (arrSin[x]*a if bit else arrCos[x]*a)
    arr[x] = arr[x]* ( maxVal if maxVal != 0 else 1)

  return arr

def detectBit(arr):
  arrSin = []
  arrCos = []
  n = len(arr)
  maxVal = max(arr)
  if not maxVal:
    maxVal = 1
  for x in range(n):  
    p = pattern(x/n)
    arrSin.append(p)
    arrCos.append(-p)
    arr[x] = arr[x]/maxVal

  c = pearson(arr, arrSin)
  corrSin =  1 if c > 0 else 0
  return corrSin


def restoreMessage(container, window, lvl, line):
  w = Wavelet('haar')
  p = math.ceil(math.log2(len(container)))
  d = 2**p - len(container)
  container = container + [0]*d
  coeffs = w.wavede(container,lvl)

  arrCoef = []
  arrCoef = linearCoeffs(coeffs, arrCoef)
  lineSize = int(len(arrCoef)/2**lvl)
  subLine = arrCoef[lineSize*line:lineSize*(line+1):]
  msg = []
  countBit = int(len(subLine)/window)
  cWindows = int(len(container)/window)
  for i in range(countBit):
    msg.append(detectBit(subLine[i*window:(i+1)*window:]))
  return msg

def getExp(num):
  ex = 0
  number = num
  if num == 0:
    return ex
  while True:
    if math.floor(number) > 0:
      return -ex
    ex+=1
    number=num*10**ex
    
arrCoef = None
coeffs = None
def includeMessage(container, msg, window, a, lvl, line, isTest=False):
  aMax = 0.09
  if(a > aMax):
    aMax = a
  aStep = 10**(getExp(a)-1)
  w = Wavelet('haar')
  p = math.ceil(math.log2(len(container)))
  d = 2**p - len(container)
  container = container + [0]*d
  coeffs = w.wavede(container,lvl)
  arrCoef = []
  arrCoef = linearCoeffs(coeffs, arrCoef)

  lineSize = int(len(arrCoef)/2**lvl)
  subLine = arrCoef[lineSize*line:lineSize*(line+1):]

  if len(msg)*window > len(subLine):
    return False
  stegCoef = []
  countBit = len(msg)
  for i in range(countBit):
    t = -1
    lstegCoef = []
    attempt = 0
    la = a+(attempt*aStep)
    while(t != msg[i]):
      la = a+(attempt*aStep)
      if(la > aMax):
        print("FAILED INCLUDE MSG")
        return False

      lstegCoef = includeWindow(window,subLine[i*window:(i+1)*window:],msg[i],la)
      t = detectBit(lstegCoef[::])
      attempt+=1
    if attempt > 1: 
      print('i = [',i,'] try again include bit, with a =',la)
    stegCoef += lstegCoef

  stegCoef+= subLine[window*countBit::] # добавляю неиспользованный хвост
  stegCoef = arrCoef[:lineSize*(line):] + stegCoef + arrCoef[lineSize*(line+1)::] # добавляю неиспользованные линии
  delinearCoeffs(coeffs, stegCoef)

  return w.waveRestore(coeffs)



def linearCoeffs(coeffs, arr):
  

  if len(coeffs['hight']) < 1:
    arr += coeffs['coef']
    return arr

  arr = linearCoeffs(coeffs['hight'], arr)
  arr = linearCoeffs(coeffs['low'], arr)
  return arr

def delinearCoeffs(coeffs, arr):
  
  if len(coeffs['hight']) < 1:
    n = len(coeffs['coef'])
    coeffs['coef'] = arr[0:n]
    arr = arr[n::]
    return arr


  arr = delinearCoeffs(coeffs['hight'], arr)
  arr = delinearCoeffs(coeffs['low'], arr)

  return arr
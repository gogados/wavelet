    const puppeteer = require('puppeteer');
    (async () => {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.setRequestInterception(true);
        
        page.on('request', request => {
            if (request.url().includes('.mp4')) {
                console.log(request.url());
                process.exit();
            } else {
                request.continue();
            }
        });
        await page.goto(process.argv[2]);
        await page.evaluate(() => {
            const el = document.querySelector('#videooverlay');
            el && el.click();
        });
        await page.waitFor(1000);
        await browser.close();
    })();
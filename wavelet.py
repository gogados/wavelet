import math
import numpy as np

class Wavelet:
  def __init__(self, fType):
    self.__mfType = fType
  
  def __getHFilterCoefs(self):
    if self.__mfType == 'dobesh':
      return [[0.6830127,1.1830127, 0.3169873, -0.1830127],[1,-1]]
    if self.__mfType == 'haar':
      return [[1,1],[1,-1]]

  def __getGFilterCoefs(self):
    if self.__mfType == 'haar':
      return [[1,1],[1,-1]]
    if self.__mfType == 'dobesh':
      return [[0.6830127,1.1830127, 0.3169873, -0.1830127],[1,-1]]

  def __applyHFilter(self,data, coefs):
    if len(coefs) < 1:
      return None
    result = []
    for i in range(len(data)):
      s = 0
      for j in range(len(coefs)):
        if i+j < len(data):
          s+=data[i+j]*coefs[j]
      s = s/len(coefs)
      result.append(s) 
    return result

  def __applyGFilter(self,data, coefs):
    if len(coefs) < 1:
      return None
    result = []
    for i in range(len(data)):
      s = 0
      for j in range(len(coefs)):
        if i >= j:
          s+=data[i-j]*coefs[j]
      result.append(s) 
    return result

 # прореживание делаем data[::2] [стартовый индекс:конечный индекс:шаг] если ничего не указано, то [0:-1:1]
          

  def __hFilters(self, data):
    hCoef = self.__getHFilterCoefs()
    
    hightArray = self.__applyHFilter(data,hCoef[0])
    lowArray = self.__applyHFilter(data,hCoef[1])

    return [hightArray[::2], lowArray[::2]]

  def __gFilters(self, hight, low):
    gCoef = self.__getGFilterCoefs()

    hArray = []
    lArray = []

    for i in range(len(hight)):
      hArray.append(hight[i])
      hArray.append(0)

    for i in range(len(low)):
      lArray.append(low[i])
      lArray.append(0)
    
    hightArray = self.__applyGFilter(hArray,gCoef[0])
    lowArray = self.__applyGFilter(lArray,gCoef[1])


    return [hightArray, lowArray]

  def __haarMethod(self, coeffs, level, endLevel):
    if(level == endLevel):
      return
    
    c = self.__hFilters(coeffs["coef"])
    coeffs['hight']["coef"] = c[0]
    coeffs['low']["coef"] = c[1]
    coeffs['low']['low'] = {}
    coeffs['low']['hight'] = {}
    coeffs['hight']['hight'] = {}
    coeffs['hight']['low'] = {}

    self.__haarMethod(coeffs["low"], level+1, endLevel)
    self.__haarMethod(coeffs["hight"], level+1, endLevel)

  def __recHaarMethod(self, coeffs):

    coef = []
    if len(coeffs['hight']['hight']) < 1: 
      coef = self.__gFilters(coeffs['hight']['coef'], coeffs['low']['coef'])
    else:
      coef = self.__gFilters(self.__recHaarMethod(coeffs['hight']), 
                             self.__recHaarMethod(coeffs['low']))

    result = []
    for i in range(len(coef[0])):
      result.append(coef[0][i]+coef[1][i])
    return result  
    

  def wavede(self, container, level):
    
    coeffs = {"coef": container, "low": {}, "hight": {}}
    self.__haarMethod(coeffs,0, level)
    return coeffs

  def waveRestore(self,coeffs):
    return self.__recHaarMethod(coeffs)
      

import wavelet
import coder
import math
from scipy import signal
import numpy as np
from random import random
import matplotlib.pyplot as plt
import os
from mpl_toolkits.mplot3d import Axes3D
import time
import json

def avgSqDiff(one,two):
  if len(one) != len(two):
    return -1
  s = 0
  for i in range(len(one)):
    s += (one[i]-two[i])**2
  return math.sqrt(s/len(one))

def plot3d(x,y,z):
  fig = plt.figure()
  ax = fig.gca(projection='3d')

  ax.plot(x, y, z, label='000')
  ax.legend()

  plt.show()

def pearson(x,y):
    n=len(x)
    vals=range(n)
    # Простые суммы
    sumx=sum([float(x[i]) for i in vals])
    sumy=sum([float(y[i]) for i in vals])
    # Суммы квадратов
    sumxSq=sum([x[i]**2.0 for i in vals])
    sumySq=sum([y[i]**2.0 for i in vals])
    # Сумма произведений
    pSum=sum([x[i]*y[i] for i in vals])
    # Вычисляем коэффициент корреляции Пирсона
    num=pSum-(sumx*sumy/n)
    den=((sumxSq-pow(sumx,2)/n)*(sumySq-pow(sumy,2)/n))**.5
    if den==0: return 0
    r=num/den
    return r


def testCorr():

  loop = 10**3
  for A in range(1):
    a = 0.035
    failed = 0
    for i in range(1,loop):
      n = 1024
      arr = [0]*n
      arrSin = []
      arrCos = []
      origin = [0]*n
      ans = int(round(random()))
      for x in range(n):
        arr[x] = random()
        
        arrSin.append((math.sin(2*math.pi*x/(n/2))))
        arrCos.append(-(math.sin(2*math.pi*x/(n/2))))

        w = arr[x]
        origin[x] = w
        arr[x] = w*(1-a) + (arrSin[x]*a if ans else arrCos[x]*a)

      avg = avgSqDiff(origin,arr)
      corrSin =  1 if pearson(arr, arrSin) > 0 else 0
      if i % 10 ==0:
        os.system('clear')
        print(i)
        print('AVG DIFF:',avg,'FOR a=',a,'ERROR:', failed*100/i, "%", 'COUNT',failed)


      if(corrSin != ans):
        failed +=1


    print('FOR a=',a,'ERROR:', failed*100/loop, "%", 'COUNT',failed)



def getMsg(file, window , lvl, line):
  data = wavelb.read(file)
  if( not data):
    return
  
  ld = len(data['channels'][0])
  p = math.ceil(math.log2(ld))
  d = math.floor((ld/2**lvl) / window)
  msg = coder.restoreMessage(data['channels'][0].tolist(), window,lvl, line)
  return msg[:d:]

def getContainerSize(data,window,lvl):
  ld = len(data)
  p = math.ceil(math.log2(ld))
  d = math.floor((ld/2**lvl) / window)
  return d

def writeMsg(data, window, a, lvl, line):
  if( not data):
    return
  ld = len(data['channels'][0])
  p = math.ceil(math.log2(ld))
  Ld = 2**p
  d = math.floor((ld/2**lvl) / window)
  msg = [0]*d

  for i in range(d):
    msg[i] = int(round(random()))
  out = data.copy()
  out['channels'] = data['channels'][::]
  out['channels'][0] = coder.includeMessage(data['channels'][0].tolist(), msg,window,a, lvl, line, False)
  if not out['channels'][0]:
    return False

  l = d*window
  out['channels'][0] = out['channels'][0][:ld:]
  wavelb.write('/home/denis/Документы/димплом статьи и книги/code/production/out.wav', out)

  return msg

def testInclude(window, a, lvl, line):
  data = wavelb.read('/home/denis/Документы/димплом статьи и книги/code/production/mo.wav')
  if( not data):
    return

  ld = len(data['channels'][0])
  p = math.ceil(math.log2(ld))
  Ld = 2**p
  d = math.floor((ld/2**lvl) / window)
  msg = [0]*d
  for i in range(d):
    msg[i] = int(round(random()))


  data['channels'][0] = coder.includeMessage(data['channels'][0].tolist(), msg,window, a, lvl, line)
  if not data['channels'][0]:
    print('TEST FAILED')
    return

  testMsg = coder.restoreMessage(data['channels'][0],window, lvl, line)
  testMsg = testMsg[:d:]

  error = 0
  for i in range(len(testMsg)):
    if msg[i] != testMsg[i]:
      error +=1
  print('LEN MSG:',len(testMsg), "ERRORS",error)
  return error*100/len(testMsg)

def compressFile():
  os.chdir('/home/denis/Документы/димплом статьи и книги/code/production/')
  os.system('ffmpeg -y -i out.wav -codec:a libmp3lame -q:a 0 out.mp3 >/dev/null 2>&1')
  os.system('rm out.wav')
  os.system('ffmpeg -y -i out.mp3 out.wav >/dev/null 2>&1')
  os.system('rm out.mp3')

def LocalTest(n, window, a, lvl, line):
  for i in range(n):
    print('ERROR: ',testInclude(window, a, lvl, line),'%')

def fileTest(data,window, a, lvl, line, compresses):
  msgLen = getContainerSize(data['channels'][0],window,lvl)
  msg = writeMsg(data,window,a, lvl, line)
  print(msg)
  if not msg:
    return [-1,msgLen]

  for i in range(compresses):
    compressFile()

  testMsg = getMsg('/home/denis/Документы/димплом статьи и книги/code/production/out.wav',window, lvl, line)


  error = 0
  for i in range(len(testMsg)):
    if msg[i] != testMsg[i]:
      error+=1

  return [error, len(msg)]

def testCompress(file):
  data = wavelb.read(file)['channels'][0]
  for i in range(150):
    compressFile()
  test = wavelb.read(file)['channels'][0]

  d = avgSqDiff(data,test)
  print('СКО',d)

def nikonovTest(window,lvl,a,line):
  data = wavelb.read('/home/denis/Документы/димплом статьи и книги/code/production/mo.wav')
  msg1 = writeMsg(data,window,a, lvl, line)
  print('msg 1',msg1)
  os.chdir('/home/denis/Документы/димплом статьи и книги/code/production/')
  os.system('mv out.wav out1.wav')
  msg2 = writeMsg(data,window,a, lvl, line)
  print('msg 2',msg2)
  os.system('mv out.wav out2.wav')

  data1 = wavelb.read('/home/denis/Документы/димплом статьи и книги/code/production/out1.wav')
  data2 = wavelb.read('/home/denis/Документы/димплом статьи и книги/code/production/out2.wav')

  for i in range(len(data1['channels'][0])):
    data2['channels'][0][i] = (data1['channels'][0][i]+data2['channels'][0][i])/2
  wavelb.write('/home/denis/Документы/димплом статьи и книги/code/production/nik.wav', data2)
  msg = getMsg('/home/denis/Документы/димплом статьи и книги/code/production/nik.wav',window,lvl,line)
  print('msg avg',msg)
  er1 = getError(msg,msg1)
  er2 = getError(msg,msg2)
  print('error 1',getError(msg,msg1))
  print('error 2',getError(msg,msg2))
  return er1 if er1 < er2 else er2
def getTime():
  return int(round(time.time() * 1000))

def getError(msg, tmpl):
  if len(msg) != len(tmpl):
    return -1
  e = 0
  for i in range(len(msg)):
    if msg[i] != tmpl[i]:
      e +=1
  return e/len(msg)

def main():
  data = [wavelb.read('/home/denis/Документы/димплом статьи и книги/code/production/sounds/'+str(i)+'.wav') for  i in range(1,11)]
  windows = [500]
  A = [0.005]
  n = 1
  lines = [0]
  lvls = [5]
  iters = len(windows)*len(A)*len(lines)*len(lvls)*n
  error = 0
  compresses = 1
  spedTime = -1
  msgLen = 0
  results = []

  results.append(['errors','msglen','count errors','count all bits','line','lvl','window','a','loops','failed'])
  for window in windows:
    for lvl in lvls:
      for line in lines:
        for a in A:
          error = 0
          failed = 0
          for i in range(n):
            iters-=1
            beginT = getTime()
            e = fileTest(data[math.floor(i/1)],window, a, lvl,line,compresses)
            spedTime = getTime() - beginT
            msgLen = e[1]
            if e[0] < 0:
              print('Не удалось встроить данные')
              failed +=1
              continue
            print(e[0])
            error += e[0]

            remaining = math.floor(iters*spedTime/1000)
            print('Осталось примерно:',math.floor(remaining / 60),'мин.',remaining % 60,'сек.',iters,'итераций')
          results.append([(error)/((n-failed)*msgLen),msgLen,error, msgLen*n,line,lvl, window, a,n,failed])
          with open('results.json', 'w') as f:
            json.dump(results, f)
          print('FINISH COUNT ERRORS:', (error)/((n-failed)*msgLen), ' LINE:', line, 'LVL:',lvl,'WINDOW:', window, 'a:',a,'failed:',failed)
  
  with open('results.json', 'w') as f:
    json.dump(results, f)

if __name__ == "__main__":
   main()
import wave
import numpy as np
import math
import struct

def read(file):
  types = {
    1: np.int8,
    2: np.int16,
    2: np.int16,
    4: np.int32
  }
  wav = wave.open(file)
  try:
    wav = wave.open(file)
    data = {}
    (nchannels, sampwidth, framerate, nframes, comptype, compname) = wav.getparams()

    data['nchannels'] = nchannels
    data['sampwith'] = sampwidth
    data['framerate'] = framerate
    data['nframes'] = nframes
    data['comptype'] = comptype
    data['compname'] = compname

    content = wav.readframes(nframes)
    samples = np.fromstring(content, dtype=types[sampwidth])
    data['channels'] = []
    for n in range(nchannels):
        data['channels'].append(samples[n::nchannels])

    return data
  except Exception:
    return None

def write(file, out):
  sampwidth = out['sampwith']
  framerate = out['framerate']
  obj = wave.open(file,'w')
  obj.setnchannels(1) # mono
  obj.setsampwidth(sampwidth)
  obj.setframerate(framerate)
  before = 0
  for value in out['channels'][0]:
    m = math.floor(value)
    m = (m if m > -0x7fff else before) if m < 0x7fff else before
    data = struct.pack('<h', m)
    before = m
    obj.writeframesraw( data )
  obj.close()
  return True